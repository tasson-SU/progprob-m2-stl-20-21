# Programmation Probabiliste 2020-2021 (M2 STL)

## Présentation

Le [syllabus](cours/syllabus-progprob.pdf) du module est disponible.


## Structuration du cours

Le cours a lieu le *mardi de 8h30 à 12h45*, sur [galène](https://galene.org:8443/group/prog-prob).

TD et TME ont lieu sur les sous-groupes galène de 4 personnes (lancez un dé à 6 faces pour trouver votre groupe) en utilisant le tableau partagé du groupe (vous pouvez utiliser latex pour écrire des formules).
[groupe 1](https://galene.org:8443/group/prog-prob/groupe1)
[Tableau 1](https://cocreate.csail.mit.edu/r/wTXHtwTaEn2wZzi9T)


[groupe 2](https://galene.org:8443/group/prog-prob/groupe2)
[Tableau 2](https://cocreate.csail.mit.edu/r/hJLf53ek6SLER6NnM)

[groupe 3](https://galene.org:8443/group/prog-prob/groupe3)
[Tableau 3](https://cocreate.csail.mit.edu/r/ekcxX3nHMu4eSh6oB)

[groupe 4](https://galene.org:8443/group/prog-prob/groupe4)
[Tableau 4](https://cocreate.csail.mit.edu/r/Z2bwM8bdhW2TznKTS)

[groupe 5](https://galene.org:8443/group/prog-prob/groupe5)
[Tableau 5](https://cocreate.csail.mit.edu/r/T5kcKovRJP7G6Qi8j)

[groupe 6](https://galene.org:8443/group/prog-prob/groupe6)
[Tableau 6](https://cocreate.csail.mit.edu/r/MAMy2kGDRiKAusKSG)


Le **détail de chaque séance** se trouve dans le [journal](journal.org).


## Modalités de contrôle des connaissances

La note du cours sera composée pour 20% de celle des devoirs maisons, de 40% du projet et pour 40% de celle de l'examen final.

## Organisation de ce dépôt

Ce dépôt contient tout le matériel pédagogique du cours :

- le **support du cours** ainsi que les exercices traités en td se trouvent dans le sous-répertoire [cours](cours/),

- les **énoncés de TP**, dans le sous-répertoire [TP](tp/),

- les **projets**, dans le sous-répertoire [Projets](projets/),
