open Random ;;
open Plplot;;
open Array;;

(** Changer la graine de l'aléa au début du programme**)
Random.self_init ()

(** Fonction pour tracer un histogramme 
    Exemple d'utilisation: 
    let () = histo data (abscisse_minimum) (abscisse_maximum) (nbre_bins) "myfile.svg" "#fr Étiquette des abscisses" "#fr Étiquette des ordonnées" "#fr Titre"

    Commande pour compiler le programme
    ocamlfind opt -package plplot -linkpkg -o histo fichier.ml


    Commande pour exécuter le programme
    ./histo

    Ensuite on peut afficher le svg dont on a donner le nom "myfile.svg"
 **)

let histo data xmin xmax bin fname lx ly title=
  (* Initialize plplot *)
  plsdev "svg";
  plsfnam fname;
  plinit ();

  (* Fill up data points *)
  plhist data xmin xmax bin [PL_HIST_DEFAULT]; 

  plcol0 1;
  plcol0 2;
  pllab lx ly title;

  plend ();
  ()

(*   Weighted  Histogram demo. *)

open Plplot;;

(** Function to draw a weighted histogram
    use example: 

    wdata: float * float array
    
    let () = whisto wdata xmin xmax ymin ymax "myfile.svg" "#fr xlabel" "#fr ylabel" "#fr Title"

    Commande pour compiler le programme
    ocamlfind opt -package plplot -linkpkg -o whisto fichier.ml


    Commande pour exécuter le programme
    ./whisto

    Ensuite on peut afficher le svg dont on a donner le nom "myfile.svg"
 **)

(** Fonction pour afficher un tableau **)
let print_data d =
  Printf.printf "[| ";
  iter (function (a,w) -> Printf.printf "%f,%f ; " a w) d;
  Printf.printf " |]"; ()


let whisto bin  wdata fname lx ly title=
  let sorted_data = Array.copy wdata in
  Array.sort (function (d, _) -> function (e, _) -> if (d>e) then 1 else -1) sorted_data ;
  (* let (xmin,_) = get sorted_data 0 and (xmax,_) = get sorted_data (length sorted_data -1) in *)
  let xmin =0. and xmax =1. in
  let eps = (xmax-.xmin) /. (float_of_int bin) in 
  let x = Array.init bin (function i -> float_of_int i*. eps +.xmin) in
  let weight i =
    Array.fold_left
      (function a -> function (d,w) ->
           if (d -. xmin) >= (float_of_int i)*. eps
           && (d -. xmin) <  (float_of_int (i+1))*. eps
           then  w/.eps+.a
           else a)
      0.
      sorted_data  in
  let y = Array.init bin  weight in let y0 = Array.get y 0 in
  let ymin, ymax = Array.fold_left (function (a,b) -> function x -> if x<a then (a,b) else (if x>b then (a,x) else (a,b))) (y0,y0) y in
  
  (* Initialize plplot *)
  plsdev "svg";
  plsfnam fname;
  plinit ();
  
  (* Create a labelled box to hold the plot. *)
  plenv xmin xmax 0. ymax 0 0;
  plcol0 1;
  plcol0 2;
  pllab lx ly title;
      
  (* Fill up data points *)
  plbin x y  [PL_BIN_DEFAULT]; 
  plend ();
  ();;





(** Fonction de sampling et d'inférence *)

(* sampling creates an array of n samples following law d *)
let sampling  (n:int) (d: unit -> 'a) =
  let y = Array.make n 0 in
  Array.map (function i-> d()) y;;


(* rejection_sampling creates an array of n samples satisfying 
predicate p and the model d *)

let rejection_sampling (n:int) (d:unit -> 'a)  (p:'a -> bool) =
  let y = Array.make n 0 in
  let rec aux i =
    let x = d () in 
    if p x then x else aux i
  in Array.map aux y;;
(** Définit la variable aléatoire (le modèle) associée à l'expérience de Laplace
    sur les biais de genre **)

(* Biased coin with parameter theta *)

let flip (theta:float) =
  let x = Random.float 1. in
  x < theta


(* Number of Females out of B registered births *)
  
let fBirth (theta: float) (b: int)=
  let rec aux  n acc=
  match n with
  | 0 -> 0
  | n -> if flip theta then  (aux (n-1) (acc+1))
                       else (aux (n-1) acc)
  in aux b 0;;


(* Sample *)


(* Rejection sampling beaucoup trop long, on pourrait l'améliorer avec pas une valeur exacte, mais 
un intervalle

let b = 493472 in
let f = 241945 in
let m () =
  (* prior *)
  let theta = Random.float 1. in
  fBirth theta b
in
let p (n:int) = (n==f)
in
rejection_sampling 2 m p;;
*)

(* Coefficients binomiaux *)
let binomialCoeff n p =
  let p = if p < n -. p then p else n -. p in
  let rec cm res num denum =
    (* this method partially prevents overflow.
     * float type is choosen to have increased domain on 32-bits computer,
     * however algorithm ensures an integral result as long as it is possible
     *)
    if denum <= p then cm ((res *. num) /. denum) (num -. 1.) (denum +. 1.)
    else res in
  cm 1. n 1. ;;

let b = 493. ;;
let f = 242. ;; 



let  genderBias () =
  (* choose theta from prior *)
  let theta = Random.float 1. in
  (* compute the score = p(F | theta, B) 
                       = theta**F (1-theta)**(B-F) 
  *)
  let score =  log (binomialCoeff b f) +. f*. log theta +. (b-.f) *.log (1.-.theta) 
  in  (theta,  exp score);;



(* importance_sampling creates an array of n samples from the
   posterior distribution deduced the model m that produces samples
   from a prior x together with its likelihood w=p(y|x) deduced from
   the data d. *)

let importance_sampling (n:int) (m: unit -> 'b * float) =
  let y = sampling n (function () -> m()) in
  (* multiply weights by prior(theta) sample n times theta *)
  let s = Array.fold_left (function a -> function (x, w) -> a+. w) 0. y in
  Array.map (function (x, w) -> (x, w/.s)) y;; 


let posterior = importance_sampling 10000 genderBias;;

let weighted_mean = Array.fold_left (function a -> function (theta, score) -> a+. theta *. (score)) 0. posterior;;

let xmin = 0.3 ;;
let xmax = 0.7 ;;

let () =
  whisto 1000 posterior   "laplace.svg" "#frTheta" "#frProbability" "#fr Example Posterior distribution of Bias";
  print_float weighted_mean;


