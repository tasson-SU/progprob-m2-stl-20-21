open Random
open Array

(** Changer la graine de l'aléa au début du programme**)
Random.self_init ()


(* sampling creates an array of n samples following law d *)
let sampling  (n:int) (d: unit -> 'a) =
  let y = Array.make n 0 in
  y (* CHANGE IT *)


(* Approximate mean value 
   Simulate n times the random variable given by program m
   get a float array [|d1,...,dn|] 
   Compute the mean: (Sum di)/n
*)

let mean  (n:int) (d: unit -> float) =
  0.  (* CHANGE IT *)

(* rejection_sampling creates an array of n samples satisfying 
predicate p and the model d *)

let rejection_sampling (n:int) (d:unit -> 'a)  (p:'a -> bool) =
  let y = Array.make n 0 in
  y (* CHANGE IT *)

(* importance_sampling creates an array of n samples from the
   posterior distribution deduced the model m that produces samples
   from a prior x together with its likelihood w=p(y|x) deduced from
   the data d. *)

let importance_sampling (n:int) (m: unit -> 'b * float) =
  let y = sampling n (function () -> m()) in
  y (* CHANGE IT*)


(* weighted mean    
   Simulate n times the random variable given by program m
   get a (value, score) : float * float array [|(m1,w1),...,(mn,wn)|] 
   Compute the weighted mean: (Sum mi * wi) 
   Assuming that Sum wi=1.
*)

let weighted_mean (n:int) (m: unit -> float * float) =
  0.

(* Gender Bias *)
(* We want to model Laplace problem on gender bias *)

(* Data *)

let b0 = 493472
let f0 = 241945

let t0 = 0.5

(* Modelf of female birth *) 

let fBirth (theta: float) (b: int)=
  0 (* CHANGE IT *)

(* Prior *)

let uniform () =
  0. (* CHANGE IT *)

(* Model using rejection sampling *)

let m () =
  (* Choose theta from the prior *)
  let theta = uniform () (* CHANGE IT *) in
  (* Model female births given theta *)
  fBirth theta b0 (* CHANGE IT *)  

(* Predicate when the sample matches the data *)

let p (n:int) =
  true (* CHANGE IT *)

(* Use rejection sampling to generate posterior distribution *)
let posterior0 = rejection_sampling 10 m p

(**********************************)
(* importance sampling genderBias *)


(* binomial *)
let binomialCoeff n p =
  let p = if p < n -. p then p else n -. p in
  let rec cm res num denum =
    (* this method partially prevents overflow.
     * float type is choosen to have increased domain on 32-bits computer,
     * however algorithm ensures an integral result as long as it is possible
     *)
    if denum <= p then cm ((res *. num) /. denum) (num -. 1.) (denum +. 1.)
    else res in
  cm 1. n 1. 
 
(* new data *)
let b = 493. 
let f = 241.  

let  genderBias () =
  (* choose theta from prior *)
  let theta = uniform () in
  (* compute the score : p(f | theta) = theta**F (1-theta)**(B-F)  *)
  let score = 0. (* CHANGE IT *)
  in  (theta,  score);;
     

(* Compute posterior distribution weighted by likelihood *)
let posterior = importance_sampling 500 genderBias in
(* Compute the mean value : Sum P(f_i |theta_i) * theta_i *)
weighted_mean posterior (* CHANGE IT *)
