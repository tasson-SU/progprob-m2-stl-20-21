open Random
open Array

(** Changer la graine de l'aléa au début du programme**)
Random.self_init ()


(* sampling creates an array of n samples following law d *)
let sampling  (n:int) (d: unit -> 'a) =
  let y = Array.make n () in
  y (* TO CHANGE *)


(* rejection_sampling creates an array of n samples satisfying 
predicate p and the model m *)

let rejection_sampling (n:int) (m: unit -> 'a)  (p:'a -> bool) =
  let y = Array.make n () in
  y (* TO CHANGE *)



(* Approximate mean value *)

let mean (m : unit -> 'a) (f : 'a -> float) =
  0. (* TO CHANGE *)


(* Coins *)
(** Fair *)
let coin () =
  true  (* TO CHANGE *)

(** Biased *)

let flip (theta:float) =
  true  (* TO CHANGE *)
  
(* Models *)
(** Flip n biased coin *)

let rec nflip (n:int) (theta:float) =
  let d = Array.make n true in
  d  (* TO CHANGE *)

(** Galton *)

let rec galton (n:int) =
  0  (* TO CHANGE *)


(** Biased Galton *)

let rec plinko (n:int) (theta:float) =
  0  (* TO CHANGE *)

(** Female births *)
  
let fBirth (theta: float) (b: int)=
  0  (* TO CHANGE *)


(* Simulate models, using sampling, and approximate mean value *)


(* Add conditions, using rejection sampling, and approximate mean value *)

(** more true than false **)

(** plinko with positive gain **)

(** biased more than twice **)
