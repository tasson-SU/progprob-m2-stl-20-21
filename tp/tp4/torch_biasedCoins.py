 # Inspired from Guillaume Baudart tutorial on ppl and reactive ppl
# https://www.di.ens.fr/~pouzet/cours/mpri/cours9/cours_rppl_solution.html

import torch
import numpy as np
import matplotlib.pyplot as plt
from torch.distributions import Uniform, Bernoulli, Beta, Normal


# utils


## plot

def plot_posterior(results,ax):
    gather = np.array(
        [(x, np.sum(np.array([w for (y, w) in results if y==x ])))
         for x in np.unique(results[:,0])])
    theta = gather[:, 0]
    score = gather[:, 1]
    ax.vlines(theta, 0, score)

def plot_pdf(d,ax):
    x = torch.linspace(0, 1, 1000)
    y = np.exp(d.log_prob(x))
    ax.plot(x, y, 'b')

##  handle distributions

def sample(d):
    return d.sample().item()

def observe(d, x):
    return d.log_prob(x).item()

# Model

def model_coins(data):
    theta = sample(Uniform(0, 1))
    score = 0
    for flip in data:
        score += observe(Bernoulli(theta), flip)
    return np.array([theta, np.exp(score)])

def importance_infer(model, data, n):
    results = np.array([model(data) for _ in range(n)])
    results[:,1] /= np.sum(results[:,1])
    return results

data = np.array([sample(Bernoulli(0.8)) for _ in range(50)])
results = importance_infer(model_coins, data, 500)

def mean(results):
    s = 0
    for (theta, w) in results:
        s += theta * w 
    return s

def var(results):
    m = mean(results)
    return np.sqrt(np.sum([ (theta*theta *w) for (theta,w) in results])-m*m)



# plot
fig, (ax1, ax2) = plt.subplots(2, 1)
## compare with beta
tails = np.sum(data)
heads = np.sum(1-data)

plot_pdf(Beta(tails+1, heads+1), ax1)

## results, var and mean
plot_posterior(results,ax2)
m = mean(results)
v = var(results)
ax2.vlines([m, m-v, m+v],0,0.02,'r')


plt.savefig('biasedCoins.png')

plt.close()
