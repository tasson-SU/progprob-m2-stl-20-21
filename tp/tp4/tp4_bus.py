# Inspired from Sam Staton tutorial on ppl
# at Oregon Programming Language Summer School
# https://www.youtube.com/watch?v=rZJJAobaQxM

import numpy as np
import matplotlib.pyplot as plt
from torch.distributions import Bernoulli, Beta, Uniform, Poisson, Gamma, Normal 

# Utils

def plot_data(results, ax, num_bins=50):
    ax.hist(results, num_bins)


def plot_posterior(results, ax):
    gather = np.array(
        [(x, np.sum(np.array([w for (y, w) in results if y==x ])))
         for x in np.unique(results[:,0])])
    theta = gather[:, 0]
    score = gather[:, 1]
    ax.vlines(theta, 0, score)

def plot_pdf(d, ax):
    x = torch.linspace(0, 1, 1000)
    y = np.exp(d.log_prob(x))
    ax.plot(x, y)

def sample(d):
    return d.sample().item()

def observe(d, x):
    return d.log_prob(x).item()


# Plots

## make subplosts
fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
## make a little extra space between the subplots
fig.subplots_adjust(hspace=0.5)
## adjust x size
ax1.set_xlim(0, 20)
ax2.set_xlim(-1, 2)
ax2.set_ylim(0, 1)
ax3.set_xlim(-1, 2)
ax3.set_ylim(0, 1)




# Model


## Poisson distribution

### at week end the frequency of the bus is 3 per hour

rate = 3
data = np.array([sample(Poisson(rate)) for _ in range(500)])

plot_data(data, ax1, 20)


### at week day the frequency of the bus is 10 per hour

rate = 10
data = np.array([sample(Poisson(rate)) for _ in range(500)])

plot_data(data, ax1, 20)



## importance_infer(model, data, n)
## sample n times model with weights observed from data
## compute the sum of weights
## return samples with normalized weights


def importance_infer(model, data, n):
    pass


## Someone looks at her window for several hours 
## and note the number of bus per hour in an array
## buses = [5,4,6,3,7]
## We want to know if it is a weekday or a week end

## Model:
## sample day from prior Bernoulli of parameter 2/7
## if it is a week day, then rate=10 bus per hour, else rate=3 per hour
## the likelihood of observing 5 buses out of Poisson(rate)
## p(5 | Poisson(rate)) = exp(Poisson(rate).log_prog(5)) =exp(observe(Poisson(rate), 5))
## the likelihood of observing buses = [5,4,6,3,7] is np.exp(score) = prod_{bus in buses} p(bus| Poisson(rate)) 
## where score = sum_{for bus in buses} observe(Poisson(rate), bus)



def model(buses):
    pass
# data = np.array([4 for _ in range(1)])

data = np.array([4,6,7,3,8])

# results = importance_infer(model, data, 500)

# plot_posterior(results, ax2)

## Second model, the rate is approximative as it comes from
## the observation of the person usually on week day and weekends
## We incorporate it by adding uncertainty about rates

def model2(buses):
    pass

# results2 = importance_infer(model2, data, 500)

# plot_posterior(results2, ax3)





plt.savefig("tp4_bus.png")


