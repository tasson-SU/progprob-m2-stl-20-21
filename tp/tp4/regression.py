# Inspired from the article
# A Convenient Category for Higher-Order Probability Theory
# by Chris Heunen, Ohad Kammar, Sam Staton, Hongseok Yang
# https://arxiv.org/abs/1701.02547

import numpy as np
from numpy.polynomial import Polynomial
import matplotlib.pyplot as plt
from torch.distributions import Bernoulli, Beta, Uniform, Poisson, Gamma, Normal 

# Utils

def plot_data(results, ax, num_bins=50):
    ax.hist(results, num_bins)


def plot_posterior(results, ax):
    gather = np.array(
        [(x, np.sum(np.array([w for (y, w) in results if y==x ])))
         for x in np.unique(results[:,0])])
    theta = gather[:, 0]
    score = gather[:, 1]
    ax.vlines(theta, 0, score)

def plot_pdf(d, ax):
    x = torch.linspace(0, 1, 1000)
    y = np.exp(d.log_prob(x))
    ax.plot(x, y)

def sample(d):
    return d.sample().item()

def observe(d, x):
    return d.log_prob(x).item()




def importance_infer(model, data, n):
    results = np.array([model(data) for _ in range(n)])
    results[:,1] /= np.sum(results[:,1])
    return results


# Plots

## make subplosts
fig, (ax1, ax2) = plt.subplots(2, 1)
## make a little extra space between the subplots
fig.subplots_adjust(hspace=0.5)
## adjust x size
ax1.set_xlim(0, 10)
ax1.set_ylim(-5, 10)
# ax2.set_xlim(0, 10)
ax2.set_ylim(-5, 20)


# Model

## Data = array of points
data = np.array([(1, 2.5), (2, 3.8), (3, 4.5), (5, 8)])
x = data[:,0]
y = data[:,1]
ax1.scatter(x,y)


## Linear regression
## Prior = Choose slop and base point randomly from a centered normal distribution
## and deduce function
## Likelihood = P ( (x,y) | f) = exp (observe(Normal((f(x)), 0.5), y))
## score = sum_{(x,y) in data} observe(Normal((f(x)), 0.5), y) 
## P( data | f) = np.exp(score)

def model(data):
    s = sample(Normal(0, 3))
    b = sample(Normal(0, 3))
    def f(x):
        return s * x + b
    score = 0
    for (x, y) in data:
        score += observe(Normal((f(x)), 0.5), y)
    return np.array([(s,b), np.exp(score)])



results = importance_infer(model, data, 500)

def f_mean(x):
    m = 0
    for ((s,b), w) in results :
        m +=(s *x+b) * w
    return m

t = np.arange(0.0, 10.0, 0.1)
ax1.plot(t, f_mean ( t), 'r--')

for (s,b), w in results:
    ax1.plot(t, s * t + b, 'b', linewidth=w*3)



# Second model with a polynomial:
# f(x)= ax^2 + bx + c

mon = np.array([5,2,3])

def pol(x, mon):
    r = 0
    for i, a in enumerate(mon):
        r += a* x**i 
    return r

# data = np.array([(0, 0), (1,1)])
data = np.array([(0.5, 12), (2, 3.8), (3, 0), (5, 8), (7,14)])
x = data[:,0]
y = data[:,1]
ax2.scatter(x,y)


def importance_infer(model, data, n):
    results = np.array([model(data) for _ in range(n)])
    s = np.sum(results[:,1])
    print(s)
    results[:,1] = results[:,1]/s
    return results



def model2(data):
    mons = np.array([sample(Uniform(-10,10)) for _ in range(3)])
    P=Polynomial(mons)
    score = 0
    for (x, y) in data:
        z = observe(Normal(P(x), 2), y)
        score += z
    return np.array([mons, np.exp(score)])

results = importance_infer(model2, data, 50000)

mean = [0]*len(results[0,0])
for mons, w in results:
    for i in range(len(mons)):
        mean[i] += mons[i] * w

print(mean)


def pol_mean (x, results) :
    s = 0
    for mons, w in results:
        s += pol(x, mons) * w
    return s


t = np.arange(0.0, 10.0, 0.1)

# for mons, w in results:
#     z = np.array([pol(x, mons) for x in t])
#     ax2.plot(t, z, 'b', linewidth=w*3)

Pm = Polynomial(mean)
z = np.array([Pm(x) for x in t])
ax2.plot(t, z, 'r--')
    
plt.savefig("regression.png")
