# Inspired from Sam Staton tutorial on ppl
# at Oregon Programming Language Summer School
# https://www.youtube.com/watch?v=rZJJAobaQxM

import numpy as np
import matplotlib.pyplot as plt
import torch
from torch.distributions import Bernoulli, Beta, Uniform, Poisson, Gamma, Normal, Exponential

# Utils

def plot_data(results, ax, num_bins=50):
    ax.hist(results, num_bins)


def plot_posterior(results, ax):
    gather = np.array(
        [(x, np.sum(np.array([w for (y, w) in results if y==x ])))
         for x in np.unique(results[:,0])])
    theta = gather[:, 0]
    score = gather[:, 1]
    ax.vlines(theta, 0, score)

def plot_pdf(d, xm ,xM, ax):
    x = torch.linspace(xm, xM, 1000)
    y = np.exp(d.log_prob(x))
    ax.plot(x, y)

def sample(d):
    return d.sample().item()

def observe(d, x):
    return d.log_prob(x).item()

def importance_infer(model, data, n):
    results = np.array([model(data) for _ in range(n)])
    results[:,1] /= np.sum(results[:,1])
    return results

# Plots

## make subplosts
fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2)
## make a little extra space between the subplots
fig.subplots_adjust(hspace=0.5)
## adjust x size
ax1.set_xlim(0, 20)
ax2.set_xlim(-1, 2)
ax2.set_ylim(0, 1)
ax3.set_xlim(0, 15)
ax3.set_ylim(0, 0.5)
ax4.set_xlim(-1, 2)
ax4.set_ylim(0, 1)
ax5.set_xlim(0, 0.5)
ax6.set_ylim(0, 1)
ax6.set_xlim(-1, 2)


# Model


## Poisson distribution

### at week end the frequency of the bus is 3 per hour

plot_pdf(Poisson(3), 0, 20, ax1)


### at week day the frequency of the bus is 10 per hour

plot_pdf(Poisson(10), 0, 20, ax1)

## Someone look at her window for several hours 
## and note the number of bus per hour in an array
## buses = [5,4,6,3,7]
## We want to know if it is a weekday or a week end

## Model:
## sample day from prior Bernoulli of parameter 2/7
## if it is a week day, then rate=10 bus per hour, else rate=3 per hour
## the likelihood of observing 5 buses out of Poisson(rate)
## observe(Poisson(rate), 5) = p(5 | Poisson(rate)) = exp(Poisson(rate).log_prog(5))
## the likelihood of observing buses = [5,4,6,3,7]



def model(buses):
    pass

# data = np.array([5,4,6,3,7])
data = [4]

### Probability of being a week end day by sampling


# results = importance_infer(model, data, 10000)

## Plot posterior distribution
# plot_posterior(results, ax2)

## Print posterior distribution
# gather = np.array(
        # [(x, np.sum(np.array([w for (y, w) in results if y==x ])))
        # for x in np.unique(results[:,0])])

# pwe = np.sum(np.array([w for (y, w) in results if y==1 ]))
# print(pwe)

### Probability of being a week end day by direct computation
# Compute this probability using Bayes Law and print it




## Normal distribution

### Use a normal distribution to represent error on the rate


plot_pdf(Normal(3,1), 0, 20, ax3)
plot_pdf(Normal(10,1), 0, 20, ax3)


## Second model, the rate is approximative as it comes from
## the observation of the person usually on week day and weekends
## We incorporate it by adding uncertainty about rates

def model2(buses):
    pass

#results2 = importance_infer(model2, data, 500)

## Plot and print posterior result

# plot_posterior(results2, ax4)

# pwe2 = np.sum(np.array([w for (y, w) in results2 if y==1 ]))
# print(pwe2)


## Exponential distribution

### Use an exponential distribution to represent the time gap elapsing
### between to bus according to the rate.

plot_pdf(Exponential(3), 0, 5,  ax5)
plot_pdf(Exponential(10), 0, 5, ax5)


## Third model, we observe a bus every 15 minutes

def model3(times):
    pass

# data3 = [0.25]
# results3 = importance_infer(model3, data3, 10000)


## Plot and print results
# plot_posterior(results3, ax6)

# pwe3 = np.sum(np.array([w for (y, w) in results3 if y==1 ]))
# print(pwe3)

## Compute the probability of being the week end given that we have observe time elpasing between buses by direct computation using Bayes Law
## Compute this probability and print it


plt.savefig("bus.png")
plt.close()

