# Inspired from the article
# A Convenient Category for Higher-Order Probability Theory
# by Chris Heunen, Ohad Kammar, Sam Staton, Hongseok Yang
# https://arxiv.org/abs/1701.02547

import numpy as np
import matplotlib.pyplot as plt
from torch.distributions import Bernoulli, Beta, Uniform, Poisson, Gamma, Normal 

# Utils

def plot_data(results, ax, num_bins=50):
    ax.hist(results, num_bins)


def plot_posterior(results, ax):
    gather = np.array(
        [(x, np.sum(np.array([w for (y, w) in results if y==x ])))
         for x in np.unique(results[:,0])])
    theta = gather[:, 0]
    score = gather[:, 1]
    ax.vlines(theta, 0, score)

def plot_pdf(d, ax):
    x = torch.linspace(0, 1, 1000)
    y = np.exp(d.log_prob(x))
    ax.plot(x, y)

def sample(d):
    return d.sample().item()

def observe(d, x):
    return d.log_prob(x).item()


# Plots

## make subplosts
fig, (ax1, ax2) = plt.subplots(2, 1)
## make a little extra space between the subplots
fig.subplots_adjust(hspace=0.5)
## adjust x size
ax1.set_xlim(0, 10)
ax1.set_ylim(-5, 10)
# ax2.set_xlim(0, 10)
ax2.set_ylim(-5, 20)


# Model

## Data = array of points
data = np.array([(1, 2.5), (2, 3.8), (3, 4.5), (5, 8)])
x = data[:,0]
y = data[:,1]
ax1.scatter(x,y)


## importance_infer(model, data, n)
## sample n times model with weights observed from data
## compute the sum of weights
## return samples with normalized weights

def importance_infer(model, data, n):
    pass


## Linear regression
## Prior = Choose slop and base point randomly from a centered normal distribution
## and deduce function
## Likelihood = P ( (x,y) | f) = exp (observe(Normal((f(x)), 0.5), y))
## score = sum_{(x,y) in data} observe(Normal((f(x)), 0.5), y) 
## P( data | f) = np.exp(score)

def model(data):
    pass

# results = importance_infer(model, data, 500)

def f_mean(x):
    m = 0
    for ((s,b), w) in results :
        m +=(s *x+b) * w
    return m

t = np.arange(0.0, 10.0, 0.1)
# ax1.plot(t, f_mean ( t), 'r--')

# for (s,b), w in results:
    # ax1.plot(t, s * t + b, 'b', linewidth=w*3)



# Second model with a polynomial:
# f(x)= a*x^2 + b*x + c or  a*x^3 + b*x^2 + c*x + d

### a monomial is a numpy array mon of its coefficient
### polynomial function associated
def pol(x, mon):
    pass
    
data = np.array([(0.5, 12), (2, 3.8), (3, 0), (5, 8), (7,14)])
x = data[:,0]
y = data[:,1]
ax2.scatter(x,y)

def model2(data):
    pass

# results = importance_infer(model2, data, 1000)


def pol_mean (x, results) :
    s = 0
    for mons, w in results:
        s += pol(x, mons) * w
    return s

t = np.arange(0.0, 10.0, 0.1)

# for mons, w in results:
    # z = np.array([pol(x, mons) for x in t])
    # ax2.plot(t, z, 'b', linewidth=w*3)

# z = np.array([pol_mean(x, results) for x in t])
# ax2.plot(t, z, 'r--')
    
plt.savefig("tp4_regression.png")
